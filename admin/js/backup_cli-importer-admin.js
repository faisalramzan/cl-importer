(function( $ ) {
	'use strict';

	/**
	 * All of the code for your admin-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */

	$(document).ready(function() {
		$(document).on('click', '#submitUserAccess', function(e){
			e.preventDefault();
			importFromCSV();
		});
	});

	// Perform AJAX
	function importFromCSV(e) {
		var importFile = $('#csvFile')[0].files;
		var form_data = new FormData();
		form_data.append('action', 'csv_ajax_call');
		form_data.append('csv', importFile[0]);
		console.log(importFile);
		// disabled the submit button
        $("#submitUserAccess").prop("disabled", true);
		$('#message').empty();
		$.ajax({
			type: 'POST',
			dataType: 'json',
			url: ajax_object.ajax_url,
			processData: false,
			contentType: false,
			cache: false,
			data: form_data,
			enctype: 'multipart/form-data',
			beforeSend: function(){
				$('#spinner').show();
			},
			success: function(data){
				$('#spinner').hide();
				$('#message').append('<p class="success">All products have been imported successfully.</p>');
				console.log(data);
			},
            error: function (e) {
                //$("#result").text(e.responseText);
                console.log("ERROR : ", e);
                $("#submitUserAccess").prop("disabled", false);

            }
		});
	};

})( jQuery );
